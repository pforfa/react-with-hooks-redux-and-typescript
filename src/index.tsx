import ReactDOM from 'react-dom';

import UserSearch from './refs/UserSearch';

const App = (): JSX.Element => {
    return <div>
        <UserSearch />
    </div>
}

ReactDOM.render(
    <App />,
    document.querySelector('#root')
);